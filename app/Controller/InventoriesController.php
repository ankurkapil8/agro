<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class InventoriesController extends AppController {
    public $components = array('Paginator', 'Session', 'Flash');
    public $uses = array('User','City','Product','Inventory');
    public function add() {
        $this->layout="farmer";
        $products = $this->Product->find('list',array('fields'=>array('id','name')));
        $this->set("products",$products);
        if($this->request->is('post')){
            $this->request->data['Inventory']['farmer_id'] = $this->Auth->user('id');
            if($this->Inventory->save($this->request->data)){
                $this->setMessage('Inventory added successfully.', 'success');
            }
        }
    }
    public function listInventory() {
        $this->layout="farmer";
        $userId = $this->Auth->user('id');
        $inventory = $this->Inventory->find('all',array('conditions'=>array('farmer_id'=>$userId)));
        //pr($inventory);exit;
        $this->set('inventory',$inventory);
    }
}
?>
