<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	public $uses = array('User','City');

/**
 * index method
 *
 * @return void
 */
	public function beforeFilter() {
        parent::beforeFilter();
		$this->Auth->allow();    
	}
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}
	public function login() {
		if($this->request->is('post')){
			if ($this->Auth->login()) {
				$this->responseArray['results']= $this->Auth->user();
	        if (isset($this->requestArray['ContentType']) && $this->requestArray['ContentType'] == 'text/html' ) {
	        	$userType = $this->Auth->user('type');
	        	if($userType == 'farmer'){
                	$this->redirect(array("controller" => "farmers", "action" => "index"));

	        	}else{
	        		$this->redirect(array("controller" => "products", "action" => "index"));
	        	}
	        }

			}else{
				$this->responseArray['status'] = false;
				$this->responseArray['message'] = "Wrong email or password";
				$this->setMessage('Wrong email or password.', 'success');

			}
		}
	}
	public function register() {
		$this->responseArray['results']= $this->City->find('list',array('id','city'));
		if($this->request->is('post')){
			$this->requestArray['User']['password']=$this->Auth->password($this->requestArray['User']['password']);
			$data = $this->User->register($this->requestArray);
			$this->responseArray = array_merge($this->responseArray, $data);
            if (isset($this->requestArray['ContentType']) && $this->requestArray['ContentType'] == 'text/html' && $this->responseArray["status"]) {
                $this->setMessage('Your register successfully.', 'success');
                $this->redirect(array("controller" => "Users", "action" => "login"));
                return;
            }

		}


	}
        public function listUser() {
            $this->layout="admin";
            $users = $this->User->find('all');
            $this->set('users',$users);
        }
    public function logout()
    {
        try {
            $logoutUrl = $this->Auth->logout();
            $this->responseArray["status"] = true;
            $this->responseArray["message"]["success"] = "You are logged out successfully.";
        } catch (Exception $e) {
            $this->responseArray["message"]["error"] = $e->getMessage();
        }
        if (in_array($this->responseArray['ResponseType'], array("XML", "JSON"))) {
            return;
        }
        $this->redirect($logoutUrl);
    }
    public function changeDeviceToken(){
    	if($this->request->is('post')){

    		$userId = $this->Auth->user('id');
    		if(empty($userId)){
    			$this->responseArray["message"] = 'user not logedin';
    			$this->responseArray["status"] = false;
    		}else{
	    		$deviceToken = $this->requestArray['device_token'];
	    		if(!empty($deviceToken)){
		    		$this->User->updateAll(array('device_token'=>$deviceToken),array('User.id'=>$userId));
	    			$this->responseArray["message"] = 'Token changed';
	    			$this->responseArray["status"] = true;

	    		}else{
	    			$this->responseArray["message"] = 'device token should not null';
	    			$this->responseArray["status"] = false;

	    		}

    		}
    	}
    }

}
