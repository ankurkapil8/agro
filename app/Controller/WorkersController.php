<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class WorkersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	public $uses = array('Worker','City');

/**
 * index method
 *
 * @return void
 */
	public function beforeFilter() {
        parent::beforeFilter();
		$this->Auth->allow();    
	}
        
        public function addWorker(){
            $this->layout="farmer";
            if($this->request->is('post')){
                $this->request->data['Worker']['farmer_id'] = $this->Auth->user('id');
                if($this->Worker->save($this->request->data)){
                    $this->setMessage('Worker deleted successfully.', 'success'); 
                    $this->redirect(array("controller" => "workers", "action" => "listWorker"));	
                }
            }
        }
        public function listWorker(){
            $this->layout = "farmer";
            $worker = $this->Worker->find('all');
            //$this->set($farms);
            $this->set('worker', $worker);
        }
        
}?>