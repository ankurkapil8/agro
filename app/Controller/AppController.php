<?php
//header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
//header("Cache-Control: post-check=0, pre-check=0", false);
//header("Pragma: no-cache");
ini_set('memory_limit', '1024M');
set_time_limit ( 0 );

/**
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
class AppController extends Controller {

    public $components  = array(
        "Auth" => array(
                "authenticate"=>array(
                    'Form' => array(
                        'fields' => array(
                                            'username' => 'email',
                                            'password' => 'password'
                                        ),
                        'userModel' => 'User'
                    )
                )
            ),
        "Session",
        'RequestHandler'
        );
    
    public $helpers=array("Html", "Form", "Session");

    public $requestArray      =   array();
    public $responseArray     =   array(    "code"              => false, 
                                            "message"           => array("success"=>"","error"=>"","info"=>""), 
                                            "status"            => false, 
                                            "validationError"   => array(), 
                                            "results"           => array()
    );

    /**
     * @author: Saurabh Kumar
     * @created: 06-01-15
     * @objective: parse request and detect request type and response type
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = "default";
        $this->Auth->loginAction    = array("controller"=>"Users", "action"=>"login");
        $this->Auth->loginRedirect  = array("controller"=>"Users", "action"=>"home");
        $this->Auth->logoutRedirect = array("controller"=>"Users", "action"=>"login");
        list($requestContent)   = explode(';', env('CONTENT_TYPE'));
        $responseAccept         = $this->request->parseAccept();
        $responseContent        = "";
        if( is_array($responseAccept) && array_key_exists('1.0', $responseAccept) )
        {
            $responseAccept =   $responseAccept['1.0'];
            if( is_array($responseAccept) && array_key_exists('0', $responseAccept) ){
                $responseContent    = trim($responseAccept['0']);
            }
        }
        
		
        try
        {
            switch(trim($requestContent))
            {
                case 'application/xml':
                case 'text/xml':
                    $this->requestArray                   = $this->decodeXML();
                    $this->requestArray                   = array_merge($this->requestArray,$this->request->data,$this->request->query);
                    $this->requestArray['RequestType']    = "XML";
                    break;

                case 'application/json':
                case 'text/x-json':
                    $this->requestArray                   = $this->request->input('json_decode',true);
                    $this->requestArray['RequestType']    = "JSON";
                    
                    
                    break;

                case 'application/soap+xml':
                    $this->requestArray                   = $this->decodeSOAP();
                    $this->requestArray['RequestType']    = "SOAP";
                    break;

                default:
                    $this->requestArray                   = array_merge($this->requestArray,$this->request->data,$this->request->query);
                    $this->requestArray['RequestType']    = "HTML";
                    $requestContent			= "text/html";
            }

            $this->requestArray['ContentType']    = $requestContent;
            $this->requestArray['RequestUrl']     = "http://".$_SERVER['SERVER_NAME'].$this->request->here;
            $this->requestArray['RemoteIp']       = $_SERVER['REMOTE_ADDR'];        

            CakeLog::write("request", var_export($this->requestArray, true) );

            switch(trim($responseContent))
            {
                case 'application/x-www-form-urlencoded':
                    $this->responseArray['ResponseType']      = "HTML";
                    $this->responseArray['ResponseContent']   = $responseContent;
                    break;

                case 'application/xml':
                case 'text/xml':
                    $this->responseArray['ResponseType']      = "XML";
                    $this->responseArray['ResponseContent']   = $responseContent;
                    
                    break;

                case 'application/json':
                case 'text/x-json':
                    $this->responseArray['ResponseType']      = "JSON";
                    $this->responseArray['ResponseContent']   = $responseContent;
                    
                    break;

                case 'application/soap+xml':
                    $this->responseArray['ResponseType']      = "SOAP";
                    $this->responseArray['ResponseContent']   = $responseContent;
                    break;

                default:
                    $this->responseArray['ResponseType']      = $this->requestArray['RequestType'];
                    $this->responseArray['ResponseContent']   = trim($requestContent);
            }
            if(isset($this->responseArray['ResponseType']) && in_array($this->responseArray['ResponseType'],array("XML","JSON"))) {
                $this->Auth->allow();
                $this->layout = $this->autoLayout = false;
                    
                    //---------auto login for device after validate------
                    $this->loadModel('User');
                    $headers = "";
                    
                    if (!function_exists('getallheaders'))
                    {
                    	$headers = $this->getAllHeaders();
                    }
                    else
                    {
                    	$headers = getallheaders();
                    }

                    if(!empty($headers['token'])) {
                        $user = $this->User->validateToken($headers['token']);
                        
                        if($user) {
                            $this->Auth->login( $user['User'] );                            

                        } else {
                            throw new Exception("User not authenticated!");
                        }

                        $this->responseArray['token'] = $headers['token'];
                    }
                    
//                    $this->Auth->allow();
            }
        }
        catch(Exception $e)
        {
                $this->responseArray["status"] = false;
                $this->responseArray["message"]["error"] = $e->getMessage();
        }
        //$this->disableCache();
    }
    

    /**
     * @author: Saurabh Kumar
     * @created: 06-01-15
     * @objective: set request and response data for view
     */
    Public function beforeRender(){
                
        $this->set('requestArray',$this->requestArray);
        $this->set('responseArray',$this->responseArray);
        if( isset($this->responseArray['ResponseType']) ) {
            if(trim($this->responseArray['ResponseType'])=='XML'){
                $this->viewClass = 'Xml';
                $this->set("_serialize", array("responseArray","requestArray"));
            }
            if( trim($this->responseArray['ResponseType'] ) == 'JSON' ){
                   $this->viewClass = 'Json';
                   $this->set("_serialize", array("responseArray","requestArray"));
            }
        }
    }
        
    
    /**
     * @author: Saurabh Kumar
     * @created: 06-01-15
     * @objective: decode xml request data into array
     * @return: array of request parameters
     */
    Protected Function decodeXML(){
        try{
            $XMLData    = $this->request->input('Xml::build');
        }catch (Exception $e){}
        
        if(!is_object($XMLData) || is_null($XMLData)){
            return array();
        }
        
        $XMLArr     = Xml::toArray($XMLData);
        if(!is_array($XMLArr)){
            throw new Exception('REQUEST NOT IN PROPER FORMAT.');
        }
        
        if(count($XMLArr)==1){
            return current($XMLArr);
        }else{
            return array();
        }
    }
    
    Protected Function decodeSOAP(){
        exit('REQUEST NOT IN PROPER FORMAT.');
    }
    
    public function setMessage($message, $type = 'error')
    {
        if($type == 'success')
            $style = 'success-message';
        else
            $style = 'error-message';
        
        $this->Flash->success($message);
    }
    
    protected function getAllHeaders()
    {
    	$headers = '';
    	 
    	foreach ($_SERVER as $name => $value)
    	{
    		if (substr($name, 0, 5) == 'HTTP_')
    		{
    			$headers[str_replace(' ', '-', strtolower(strtolower
    					(str_replace('_', ' ', substr($name, 5)))))] = $value;
    		}
    	}
    	 
    	return $headers;
    }

    /**
     * @return string base url
     */
    public function getBaseUrl() {
        $http_host = array();
        $http_host = explode(':', $_SERVER['HTTP_HOST']);
        if($http_host[0]== 'mybestconnect.bestseller.com')
            $protocol = 'https';
        else
            $protocol = 'http';
//        $protocol = isset($_SERVER["https"]) ? 'https' : 'http';
        return $protocol.'://'.$http_host[0].$this->base.'/';
    }
}
