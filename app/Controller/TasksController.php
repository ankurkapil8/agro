<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class TasksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	public $uses = array('Worker','Task');

/**
 * index method
 *
 * @return void
 */
	public function beforeFilter() {
        parent::beforeFilter();
		$this->Auth->allow();    
	}
        public function addTask() {
            $this->layout="farmer";
            $workers = $this->Worker->find('list',array('id','name'));
            if($this->request->is('post')){
                if($this->Task->save($this->request->data)){
                    $this->setMessage('Task added successfully.', 'success'); 
                   // $this->redirect(array("controller" => "tasks", "action" => "listTasks"));	
                }
            }
            $this->set("workers",$workers);
        }
        public function listTask(){
            $this->layout = "farmer";
            $task = $this->Task->find('all');
            $this->set('task', $task);
        }
        
}?>