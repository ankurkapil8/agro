<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class ProductsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	public $uses = array('User','City','Product');
	public function index(){
		$this->layout = "admin";
	}
	public function addProduct(){
		$this->layout = "admin";
                //pr($this->request->data);exit;
                if($this->request->is('post')){
                    if (
                        !empty($this->request->data['Product']['image']['tmp_name'])
                        && is_uploaded_file($this->request->data['Product']['image']['tmp_name'])
                    ) {
                        // Strip path information
                        $filename = WWW_ROOT . 'img/products' . DS . rand() . "_" . time() . '.' . pathinfo($this->request->data['Product']['image']['name'], PATHINFO_EXTENSION);
                        move_uploaded_file(
                            $this->data['Product']['image']['tmp_name'],
                            $filename
                        );
                    }
                    
                    $this->request->data['Product']['image'] = str_replace(DS, "/", str_replace(WWW_ROOT, "app/webroot/", $filename));;
                    //pr($this->request->data);exit;
                    if($this->Product->save($this->request->data)){
                        $this->setMessage('Product added successfully.', 'success');
                    }
                }
               
	}
        public function listProduct() {
            $this->layout = "admin";
            $products = $this->Product->find('all');
            $this->responseArray['status'] = true;
            $this->responseArray['results'] = $products;
        }
}?>
