<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class FarmersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	public $uses = array('User','Farm');
	
/**
 * index method
 *
 * @return void
 */
	public function beforeFilter() {
        parent::beforeFilter();
		$this->Auth->allow();    
	}
	public function index(){
		$this->layout = "farmer";
	}
	public function addFarm(){
		$this->layout = "farmer";
		if($this->request->is('post')){
			$data = $this->request->data;
                        $data['Farm']['farmer_id'] = $this->Auth->user('id');
                        $this->Farm->save($data);
                        $this->setMessage('Farm added successfully.', 'success');
                        $this->redirect(array("controller" => "Farmers", "action" => "listFarm"));			
		}
	}
        public function listFarm(){
            $this->layout = "farmer";
            $farms = $this->Farm->find('all');
            //$this->set($farms);
            $this->set('farms', $farms);
        }
        public function deleteFarm($id){
            if($this->Farm->delete($id)){
               $this->setMessage('Farm deleted successfully.', 'success'); 
               $this->redirect(array("controller" => "Farmers", "action" => "listFarm"));	
            }
        }
}
?>