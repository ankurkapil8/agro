<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class OrdersController extends AppController {
    public $components = array('Paginator', 'Session', 'Flash');
    public $uses = array('User','City','Product','Inventory','Order','Sale');
	public function beforeFilter() {
        parent::beforeFilter();
		$this->Auth->allow();    
	}
    public function placeOrder() {
        $this->responseArray['results']['City'] = $this->City->find('list');
        if($this->request->is('post')){
            $status =$this->Order->placeOrder($this->request->data);
            if($status){
                $this->responseArray['status'] = true;
                $this->responseArray['message'] = "order pleace successfully";
                
            }else{
                $this->responseArray['status'] = false;
                $this->responseArray['message'] = "Something went wrong";
                
            }
        }
    }
    public function getOrder() {
        $this->layout="farmer";
        $userDetail = $this->Auth->user();
        $orderDetails = $this->Order->getOrder($userDetail);
        $this->responseArray['status'] = true;
        $this->responseArray['results']=$orderDetails;
    }
    public function supply($orderId){
        $this->layout="farmer";
        $this->Sale->AddSale($orderId);
        $this->setMessage('Order status changed.');
        $this->redirect(array("controller" => "orders", "action" => "getOrder"));	
    }
    public function filledOrder() {
        $this->layout="farmer";
        $filledOrder = $this->Order->find('all',array('conditions'=>array('farmer_id'=>$this->Auth->user('id'))));
        $this->set('filledOrder',$filledOrder);
        
    }
    public function sendPush() {
        $this->Order->sendNotificationAndroid();
        exit;
    }
}?>