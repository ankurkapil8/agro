<?php 
$city=$responseArray["results"];['city'];
?>
<div class="card card-login mx-auto mt-5">
        <div class="card-header">
          Register
        </div>
        <div class="card-body">
    
    <?php  echo $this->Form->create('User',array('class'=>''));
?>
      <div class="form-group">
<label for="exampleInputEmail1">Name</label>
      <?php echo $this->Form->input('name',array("placeholder"=>'name','div'=>false,'label'=>false, 'class'=>'form-control')); ?>
      </div>
      <div class="form-group">
<label for="exampleInputEmail1">Email</label>
      <?php echo $this->Form->input('email',array("placeholder"=>'email address','div'=>false,'label'=>false, 'class'=>'form-control')); ?>
      </div>
      <div class="form-group">
<label for="exampleInputEmail1">Password</label>
      <?php echo $this->Form->input('password',array('type'=>'password',"placeholder"=>'password','div'=>false,'label'=>false, 'class'=>'form-control')); ?>
      </div>
      <div class="form-group">
<label for="exampleInputEmail1">Country</label>
      <?php echo $this->Form->input('country',array('type'=>'select','options'=>array(''=>'Select Country','SA'=>'South African'),'div'=>false,'label'=>false,'default'=>'', 'class'=>'form-control')); ?>
      </div>
      <div class="form-group">
<label for="exampleInputEmail1">City</label>
      <?php echo $this->Form->input('city',array('type'=>'select','options'=>$city,'div'=>false,'label'=>false,'default'=>'','empty'=>'select City','class'=>'form-control')); ?>
            <div class="form-group">
<label for="exampleInputEmail1">Address</label>
      <?php echo $this->Form->input('address',array("placeholder"=>'address','div'=>false,'label'=>false,'class'=>'form-control')); ?>
      </div>
      <div class="form-group">
<label for="exampleInputEmail1">City</label>
      <?php echo $this->Form->input('type',array('type'=>'select','options'=>array(''=>'Select type','2'=>'farmer'),'div'=>false,'label'=>false,'default'=>'','class'=>'form-control')); ?>
      </div>
      <button type="submit" class="btn btn-primary btn-block">create</button>
    <?php echo $this->Form->end(); ?>
  </div>
      </div>
