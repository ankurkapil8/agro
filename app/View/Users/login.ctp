  <div class="card card-login mx-auto mt-5">
        <div class="card-header">
          Login
        </div>
        <div class="card-body">
  <?php  echo $this->Form->create('User',array('class'=>''));
?>
<div class="form-group">
<label for="exampleInputEmail1">Email address</label>
      <?php echo $this->Form->input('email',array("placeholder"=>'email address','div'=>false,'label'=>false,'class'=>'form-control')); ?>
      </div>
      <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
      <?php echo $this->Form->input('password',array('type'=>'password',"placeholder"=>'password','div'=>false,'label'=>false,'class'=>'form-control')); ?>
      </div>
      <button type="submit" class="btn btn-primary btn-block">login</button>
      <?php echo $this->Form->end(); ?>
      <div class="text-center">
            <a class="d-block small mt-3" href="register">Register an Account</a>
            <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
          </div>
        </div>
      </div>

      