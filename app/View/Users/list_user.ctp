<table class="table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Country</th>
        <th>Address</th>
        <th>Type</th>
      </tr>
    </thead>
    <tbody>
        <?php 
        foreach ($users as $key=>$value) {?>
        <tr class="<?php echo ($key%2==0?"success":"warning") ?>">
            <td>
                <?php echo $value['User']['name']; ?>
            </td>
            <td>
                <?php echo $value['User']['email']; ?>
            </td>
            <td>
                <?php echo $value['User']['country']; ?>
            </td>
            <td>
                <?php echo $value['User']['address']; ?>
            </td>
            <td>
                <?php echo $value['User']['type']; ?>
            </td>

        </tr>
        <?php }
        ?>
    </tbody>
  </table>