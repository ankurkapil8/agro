<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">List Farm</li>
        </ol>
<table class="table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Address</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        <?php 
        foreach ($farms as $key=>$value) {?>
        <tr class="<?php echo ($key%2==0?"success":"warning") ?>">
            <td>
                <?php echo $value['Farm']['name']; ?>
            </td>
            <td>
                <?php echo $value['Farm']['address']; ?>
            </td>
            <td>
                <?php echo $this->html->link('Delete', array('controller'=>'Farmers', 'action' => 'deleteFarm',$value['Farm']['id']), array('title'=>''));?>
            </td>

        </tr>
        <?php }
        ?>
    </tbody>
  </table>