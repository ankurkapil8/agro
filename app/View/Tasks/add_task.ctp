<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Assign Task</li>
        </ol> <?php  echo $this->Form->create('Task',array('class'=>'form-horizontal'));
?>
  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Worker:</label>
    <div class="col-sm-4">
    <?php echo $this->Form->input('worker_id',array('type'=>'select','options'=>$workers,'div'=>false,'label'=>false,'default'=>'','empty'=>'select Worker')); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Task Details:</label>
    <div class="col-sm-4">
    <?php echo $this->Form->input('task_detail',array("placeholder"=>'Description','div'=>false,'label'=>false,'class'=>'form-control')); ?>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
  <button type="submit" class="btn btn-success">Assign Task</button>
  </div>
  </div>
<?php echo $this->Form->end(); ?>