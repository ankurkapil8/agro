<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">List Task</li>
        </ol><table class="table">
    <thead>
      <tr>
        <th>Task Details</th>
        <th>Worker</th>
      </tr>
    </thead>
    <tbody>
        <?php 
        foreach ($task as $key=>$value) {?>
        <tr class="<?php echo ($key%2==0?"success":"warning") ?>">
            <td>
                <?php echo $value['Task']['task_detail']; ?>
            </td>
            <td>
                <?php echo $value['Worker']['name']; ?>
            </td>

        </tr>
        <?php }
        ?>
    </tbody>
  </table>