<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">List Product</li>
        </ol><?php 
$products = $responseArray['results'];
//pr($products);exit;
?>
<table class="table">
    <thead>
      <tr>
        <th>Image</th>
        <th>Name</th>
        <th>Description</th>
        <th>Price(ZAR)</th>
      </tr>
    </thead>
    <tbody>
        <?php 
        foreach ($products as $key=>$value) {?>
        <tr class="<?php echo ($key%2==0?"success":"warning") ?>">
            <td>
                <?php echo $this->Html->image("/".$value['Product']['image'],array('height'=>'25%','width'=>'25%')); ?>
            </td>
            <td>
                <?php echo $value['Product']['name']; ?>
            </td>
            <td>
                <?php echo $value['Product']['description']; ?>
            </td>
            <td>
                <?php echo $value['Product']['price']; ?>
            </td>

        </tr>
        <?php }
        ?>
    </tbody>
  </table>