<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Add Product</li>
        </ol> <?php  echo $this->Form->create('Product',array('class'=>'form-horizontal', 'type' => 'file'));
?>
  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Product Name:</label>
    <div class="col-sm-4">
    <?php echo $this->Form->input('name',array("placeholder"=>'name','div'=>false,'label'=>false,'class'=>'form-control')); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Product Description:</label>
    <div class="col-sm-4">
    <?php echo $this->Form->input('description',array("placeholder"=>'description','div'=>false,'label'=>false,'class'=>'form-control')); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Product Image:</label>
    <div class="col-sm-4">
    <?php echo $this->Form->input('image', array('type' => 'file','label' => false,'div'=>false)); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Price:</label>
    <div class="col-sm-4">
    <?php echo $this->Form->input('price',array("type"=>'number','div'=>false,'label'=>false,'class'=>'form-control')); ?>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
  <button type="submit" class="btn btn-success">Add</button>
  </div>
  </div>
<?php echo $this->Form->end(); ?>