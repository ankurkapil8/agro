<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">List Worker</li>
        </ol><table class="table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Phone</th>
        <th>Address</th>
      </tr>
    </thead>
    <tbody>
        <?php 
        foreach ($worker as $key=>$value) {?>
        <tr class="<?php echo ($key%2==0?"success":"warning") ?>">
            <td>
                <?php echo $value['Worker']['name']; ?>
            </td>
            <td>
                <?php echo $value['Worker']['phone']; ?>
            </td>
            <td>
                <?php echo $value['Worker']['address']; ?>
            </td>

        </tr>
        <?php }
        ?>
    </tbody>
  </table>