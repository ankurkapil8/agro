<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Add Worker</li>
        </ol>
 <?php  echo $this->Form->create('Worker',array('class'=>'form-horizontal'));
?>
  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Name:</label>
    <div class="col-sm-4">
    <?php echo $this->Form->input('name',array("placeholder"=>'name','div'=>false,'label'=>false,'class'=>'form-control')); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Phone:</label>
    <div class="col-sm-4">
    <?php echo $this->Form->input('phone',array("placeholder"=>'Phone','div'=>false,'label'=>false,'class'=>'form-control')); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="pwd" class="control-label col-sm-2">Address:</label>
    <div class="col-sm-4">
    <?php echo $this->Form->input('address',array("placeholder"=>'address','div'=>false,'label'=>false,'class'=>'form-control')); ?>
    </div>
  </div>
   <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
  <button type="submit" class="btn btn-success">Add</button>
  </div>
  </div>
<?php echo $this->Form->end(); ?>