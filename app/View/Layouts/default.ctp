
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Agroare</title>

    <!-- Bootstrap core CSS -->
<!--     <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 -->    <?php echo $this->Html->css('bootstrap.min.css'); ?>

    <!-- Custom fonts for this template -->
   <!--  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
 <?php echo $this->Html->css('font-awesome.min.css'); ?>
    <!-- Custom styles for this template -->
<!--     <link href="css/sb-admin.css" rel="stylesheet">
 --><?php echo $this->Html->css('sb-admin.css'); ?>
  </head>

  <body class="bg-dark">

    <div class="container">

			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
    </div>

    <!-- Bootstrap core JavaScript -->
    <?php echo $this->Html->script('jquery.min.js'); ?>
    <?php echo $this->Html->script('popper.min.js'); ?>
    <?php echo $this->Html->script('bootstrap.min.js'); ?>
  </body>

</html>
