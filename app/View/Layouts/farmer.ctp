
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Agroare</title>

    <!-- Bootstrap core CSS -->
<!--     <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 -->    <?php echo $this->Html->css('bootstrap.min.css'); ?>

    <!-- Custom fonts for this template -->
   <!--  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
 <?php echo $this->Html->css('font-awesome.min.css'); ?>
    <!-- Custom styles for this template -->
<!--     <link href="css/sb-admin.css" rel="stylesheet">
 --><?php echo $this->Html->css('sb-admin.css'); ?>
  </head>
  <body class="fixed-nav sticky-footer bg-dark" id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <a class="navbar-brand" href="#">Agroare</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
          <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Dashboard">
            <a class="nav-link" href="#">
              <i class="fa fa-fw fa-dashboard"></i>
              <span class="nav-link-text">
                Dashboard</span>
            </a>
          </li>
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text">
                Manage Farm</span>
            </a>
            <ul class="sidenav-second-level collapse" id="collapseComponents">
<li><?php echo $this->html->link('Add Farm', array('controller'=>'Farmers', 'action' => 'addFarm'), array('title'=>''));?></li>
            <li><?php echo $this->html->link('List Farm', array('controller'=>'Farmers', 'action' => 'listFarm'), array('title'=>''));?></li>
			          <li><?php echo $this->html->link('Add Worker', array('controller'=>'Workers', 'action' => 'addWorker'), array('title'=>''));?></li>
			          <li><?php echo $this->html->link('List Worker', array('controller'=>'Workers', 'action' => 'ListWorker'), array('title'=>''));?></li>
			          <li><?php echo $this->html->link('Add Inventory', array('controller'=>'inventories', 'action' => 'add'), array('title'=>''));?></li>
			          <li><?php echo $this->html->link('List Inventory', array('controller'=>'inventories', 'action' => 'listInventory'), array('title'=>''));?></li>
            </ul>
          </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents2" data-parent="#exampleAccordion">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text">
                Manage Task</span>
            </a>
            <ul class="sidenav-second-level collapse" id="collapseComponents2">
            <li><?php echo $this->html->link('Assign Task', array('controller'=>'Tasks', 'action' => 'addTask'), array('title'=>''));?></li>
                                  <li><?php echo $this->html->link('List Task', array('controller'=>'Tasks', 'action' => 'listTask'), array('title'=>''));?></li>
            </ul></li>
      
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts"><?php echo $this->html->link('Order', array('controller'=>'orders', 'action' => 'getOrder'), array('title'=>'','class'=>'nav-link'));?></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts"><?php echo $this->html->link('Sale', array('controller'=>'orders', 'action' => 'filledOrder'), array('title'=>'','class'=>'nav-link'));?></li>          
          

        </ul>
        <ul class="navbar-nav sidenav-toggler">
          <li class="nav-item">
            <a class="nav-link text-center" id="sidenavToggler">
              <i class="fa fa-fw fa-angle-left"></i>
            </a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          
          
          <li class="nav-item">
            
<?php echo $this->html->link('Logout', array('controller'=>'users', 'action' => 'logout'), array('title'=>''));?>              
          </li>
        </ul>
      </div>
    </nav>

    <div class="content-wrapper">

      <div class="container-fluid">
    	<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
    </div>
    </div>
    <!-- /.content-wrapper -->

    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small</small>
        </div>
      </div>
    </footer>

    <!-- Scroll to Top Button -->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>

  
 

  

    <!-- Custom scripts for this template -->
	<?php echo $this->Html->script('jquery.min.js'); ?>
    <?php echo $this->Html->script('popper.min.js'); ?>
    <?php echo $this->Html->script('bootstrap.min.js'); ?>
    <?php echo $this->Html->script('sb-admin.min.js'); ?>
  </body></html>
