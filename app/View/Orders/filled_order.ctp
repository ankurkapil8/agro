<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Sales</li>
        </ol><table class="table">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Customer Name</th>
        <th>Delivered City</th>
        <th>Delivered Address</th>
        <th>Delivered Date</th>
      </tr>
    </thead>
    <tbody>
        <?php
        $total = 0;
        foreach ($filledOrder as $key=>$value) {
            $total +=$value['Order']['price']; 
            ?>
        <tr class="<?php echo ($key%2==0?"success":"warning") ?>">
            <td>
                <?php echo $value['Product']['name']; ?>
            </td>
            <td>
                <?php echo $value['Order']['quantity']; ?>
            </td>
            <td>
                <?php echo $value['Order']['price']; ?>
            </td>
            <td>
                <?php echo $value['Customer']['name']; ?>
            </td>
            <td>
                <?php echo $value['City']['city']; ?>
            </td>
            <td>
                <?php echo $value['Order']['address']; ?>
            </td>
            <td>
                <?php echo $value['Order']['created']; ?>
            </td>

        </tr>
        <?php }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6"><b>Total Sale</b></td>
            <td><?php echo $total; ?></td>
        </tr>
    </tfoot>
  </table>