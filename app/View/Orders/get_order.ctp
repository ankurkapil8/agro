<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Order List</li>
        </ol><?php 
$orderDetails = $responseArray['results'];
//pr($products);exit;
?>
<table class="table">
    <thead>
      <tr>
        <th>Product Image</th>
        <th>Product Name</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Customer Name</th>
        <th>Delivery City</th>
        <th>Delivery Address</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        <?php 
        foreach ($orderDetails as $key=>$value) {?>
        <tr class="<?php echo ($key%2==0?"success":"warning") ?>">
            <td>
                <?php echo $this->Html->image("/".$value['Product']['image'],array('height'=>'25%','width'=>'25%')); ?>
            </td>
            <td>
                <?php echo $value['Product']['name']; ?>
            </td>
            <td>
                <?php echo $value['Order']['quantity']; ?>
            </td>
            <td>
                <?php echo $value['Order']['price']; ?>
            </td>
            <td>
                <?php echo $value['Customer']['name']; ?>
            </td>
            <td>
                <?php echo $value['City']['city']; ?>
            </td>
            <td>
                <?php echo $value['Order']['address']; ?>
            </td>
            <td>
                <?php echo $this->html->link('Supply', array('controller'=>'orders', 'action' => 'supply',$value['Order']['id']), array('title'=>''));?>
            </td>

        </tr>
        <?php }
        ?>
    </tbody>
  </table>