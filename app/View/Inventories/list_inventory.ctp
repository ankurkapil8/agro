<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">List Inventory</li>
        </ol><table class="table">
    <thead>
      <tr>
        <th>Image</th>
        <th>Name</th>
        <th>Description</th>
        <th>Price(ZAR)</th>
        <th>Quantity</th>
      </tr>
    </thead>
    <tbody>
        <?php 
        foreach ($inventory as $key=>$value) {?>
        <tr class="<?php echo ($key%2==0?"success":"warning") ?>">
            <td>
                <?php echo $this->Html->image("/".$value['Product']['image'],array('height'=>'25%','width'=>'25%')); ?>
            </td>
            <td>
                <?php echo $value['Product']['name']; ?>
            </td>
            <td>
                <?php echo $value['Product']['description']; ?>
            </td>
            <td>
                <?php echo $value['Product']['price']; ?>
            </td>
            <td>
                <?php echo $value['Inventory']['quantity']; ?>
            </td>

        </tr>
        <?php }
        ?>
    </tbody>
  </table>