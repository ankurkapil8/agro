<ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Add Inventory</li>
        </ol> <?php  echo $this->Form->create('Inventory',array('class'=>'form-horizontal'));
?>
  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Product:</label>
    <div class="col-sm-4">
    <?php echo $this->Form->input('product_id',array('type'=>'select','options'=>$products,'div'=>false,'label'=>false,'default'=>'','empty'=>'select product', 'class'=>'form-control')); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Quantity:</label>
    <div class="col-sm-4">
    <?php echo $this->Form->input('quantity',array("placeholder"=>'quantity','div'=>false,'label'=>false,'class'=>'form-control')); ?>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
  <button type="submit" class="btn btn-success">Add</button>
  </div></div>
<?php echo $this->Form->end(); ?>