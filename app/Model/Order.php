<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class Order extends AppModel {
        public $belongsTo = array(
        'Product' => array(
                'className' => 'Product',
                'foreignKey' => 'product_id',
                'conditions' => '',
                'fields' => '',
                'order' => ''
        ),
        'Customer' => array(
                'className' => 'User',
                'foreignKey' => 'customer_id',
                'conditions' => '',
                'fields' => '',
                'order' => ''
        ),
        'City' => array(
                'className' => 'City',
                'foreignKey' => 'city_id',
                'conditions' => '',
                'fields' => '',
                'order' => ''
        )
            
    );
    public function placeOrder($requestArray) {
        
        if($this->save($requestArray)){
/*            $this->sendNotificationAndroid('e1JMoEC9f5E:APA91bG31OYS2zWZ4Aeotgdf11K2qa_tjn_JtbyYzbAXlVuP5Ax884h19lMQLhqtsAxFywdY1NW4--Ktj51uMDAM3wvK3KTmgNGri68olo-vImR0lkRIcgIfWYpgroC3vh6ICUIiYMh3',"hello");
*/            return true;
        }else{
            return false; 
        }
    }
    function sendNotificationAndroid($registatoin_ids=null, $message=null, $data=null)
    {
        // Set POST variables
        $apiKey = 'AAAAAPkN8fM:APA91bHBa0KJRAeINoVrFgww_L2utBImfQ74qHs8d1DajuWclB5MOjbbXX6yD3JJRA8SqzI3eBGnE54vZkrf52Womi_Rr1MUMhXxOGzoNGVZhw7u9cFhRmlLcqdvOMZ0YcYN-qOFUNu4';
        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields['notification'] = array(
            'title' => 'Agroware',
            'body' =>$message,
            'icon' =>'fcm_push_icon'
        );
        $fields['to']=$registatoin_ids;
        $headers = array(
            'Authorization: key='.$apiKey,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        echo "hello";exit;
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
pr($result);exit;
        // Close connection
        curl_close($ch);
        $resp = json_decode($result);
       if($resp->success) {
           return true;
       } else {
           return false;
       }
    }
    public function getOrder($userDetails) {
        $orderDetails = $this->find('all',array('conditions'=>array('status'=>0,'city_id'=>$userDetails['city'])));
        return $orderDetails;
    }
}
?>