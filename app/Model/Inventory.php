<?php
App::uses('AppModel', 'Model');
/**
 * City Model
 *
 */
class Inventory extends AppModel {
        public $belongsTo = array(
        'Product' => array(
                'className' => 'Product',
                'foreignKey' => 'product_id',
                'conditions' => '',
                'fields' => '',
                'order' => ''
        )
    );

}
?>