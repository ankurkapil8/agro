<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public function register($requestArray){
		$data['status'] = true;
		$requestArray['User']['auth_token'] = $this->generateToken($requestArray[$this->name]['email'],$requestArray[$this->name]['password']);
		if($this->save($requestArray)){
            $lastId = $this->getLastInsertID();
            $data['results']=$this->find('first',array('conditions'=>array('User.id'=>$lastId)));
		    $data['message'] = 'user added successfully';

		}else{
			$data['status'] = false;
		}
		return $data;
	}
    public function generateToken($username, $password, $secret = 'red')
    {
        $str = md5($username . $password);
        $token = $str . ':' . base64_encode($secret);
        $date = date('Y-m-d');
        return $token;
    }
    public function validateToken($token, $secret = 'red')
    {
        $user = $this->find('first', array(
            'conditions' => array('auth_token' => $token)
        ));

        if ($user) {


            return $user;
            // }
        }
        return false;
    }


}
