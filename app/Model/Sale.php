<?php
App::uses('AppModel', 'Model');
/**
 * City Model
 *
 */
class Sale extends AppModel {
    public function AddSale($order_id) {
        $currentDate = date('Y-m-d');
        $orderModel = ClassRegistry::init('Order');
        $farmer_id = AuthComponent::user('id');
        $orderDetails = $orderModel->find('first',array('conditions'=>array('Order.id'=>$order_id)));
        $data['Sale']['order_id'] = $order_id; 
        $data['Sale']['amount'] = $orderDetails['Order']['price']; 
        $data['Sale']['date'] = $currentDate; 
        $this->save($data);
        $orderModel->updateAll(array('farmer_id'=>$farmer_id,'status'=>1,'created'=>"'$currentDate'"),array('Order.id'=>$order_id));
        return TRUE;
    }
}
?>