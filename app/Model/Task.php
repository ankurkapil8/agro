<?php
App::uses('AppModel', 'Model');
/**
 * Farm Model
 *
 */
class Task extends AppModel {
	public $displayField = 'task_detail';
    public $belongsTo = array(
        'Worker' => array(
                'className' => 'Worker',
                'foreignKey' => 'worker_id',
                'conditions' => '',
                'fields' => '',
                'order' => ''
        )
    );
        
}
?>