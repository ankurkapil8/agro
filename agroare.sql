-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 31, 2017 at 05:21 PM
-- Server version: 5.5.55-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `agroare`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(11) NOT NULL,
  `city` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7275 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country`, `city`) VALUES
(1, 'SA', 'gqaka'),
(2, 'SA', 'gqaqala'),
(3, 'SA', 'gqiba'),
(4, 'SA', 'gqobonco'),
(5, 'SA', 'gqogqora'),
(6, 'SA', 'gqola');

-- --------------------------------------------------------

--
-- Table structure for table `farms`
--

CREATE TABLE IF NOT EXISTS `farms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `farmer_id` int(11) NOT NULL,
  `name` varchar(11) NOT NULL,
  `address` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `farms`
--

INSERT INTO `farms` (`id`, `farmer_id`, `name`, `address`) VALUES
(1, 1, 'test', 'test'),
(3, 1, '22', '22');

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE IF NOT EXISTS `inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `farmer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `farmer_id`, `product_id`, `quantity`) VALUES
(1, 1, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `farmer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `address` varchar(50) NOT NULL,
  `status` tinyint(2) NOT NULL COMMENT '0 not deliver, 1 deliver',
  `created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `farmer_id`, `product_id`, `quantity`, `price`, `city_id`, `address`, `status`, `created`) VALUES
(1, 1, 1, 1, 12, 122, 1, 'test', 1, '2017-08-31'),
(2, 1, 1, 1, 12, 122, 1, 'test', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `price` int(5) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `image`) VALUES
(1, 'test', '', 23, '/var/www/h'),
(2, 'test', 'test', 22, '/var/www/html/agro/app/webroot/img/products/1218194678_1504159617.png'),
(3, 'test', 'test', 33, 'app/webroot/img/products/1184277578_1504159721.png'),
(4, 'test', 'test', 33, 'app/webroot/img/products/1828951452_1504160426.png'),
(5, 'meat', 'meat', 120, 'app/webroot/img/products/612608407_1504160804.png');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `order_id`, `amount`, `created`) VALUES
(1, 1, 122, '2017-08-31'),
(2, 1, 122, '2017-08-31'),
(3, 1, 122, '2017-08-31'),
(4, 1, 122, '2017-08-31'),
(5, 2, 122, '2017-08-31'),
(6, 1, 122, '2017-08-31'),
(7, 1, 122, '2017-08-31'),
(8, 1, 122, '2017-08-31');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `worker_id` int(11) NOT NULL,
  `task_detail` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `worker_id`, `task_detail`) VALUES
(1, 1, 'test'),
(2, 1, 'test'),
(3, 1, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(11) NOT NULL,
  `auth_token` varchar(50) NOT NULL,
  `device_token` varchar(225) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `country` varchar(11) NOT NULL,
  `city` varchar(11) NOT NULL,
  `address` varchar(11) NOT NULL,
  `type` enum('admin','farmer','customer') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `auth_token`, `device_token`, `email`, `password`, `country`, `city`, `address`, `type`) VALUES
(1, 'ankur', 'f568720604fb10d4c4f6d36be8e806fb:cmVk', '', 'root@gmail.com', 'a9a36e3dce8c41d182588e7330dcba9405517836', 'SA', '1', 'test', 'farmer'),
(2, 'admin', '258b9456a3d79355a07d95b59f623c36:cmVk', '', 'admin@gmail.com', 'a9a36e3dce8c41d182588e7330dcba9405517836', 'SA', 'SA', 'test', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `workers`
--

CREATE TABLE IF NOT EXISTS `workers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `phone` int(20) NOT NULL,
  `address` varchar(50) NOT NULL,
  `farmer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `workers`
--

INSERT INTO `workers` (`id`, `name`, `phone`, `address`, `farmer_id`) VALUES
(1, 'test', 123456789, 'test', 1),
(2, 'test', 123456789, 'test', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
